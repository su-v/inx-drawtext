#!/usr/bin/env python
"""
Draw Text - renders text using glyph data retrieved from SVG fonts.

Copyright 2011, Windell H. Oskay, www.evilmadscientist.com
Copyright 2016, su_v, <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



How to add more SVGfont resources:

    1. Copy SVG file with SVG font definition into subdir 'drawtextdata'
    2. Add new fonts as items to 'fontface' enum parameter in drawtext.inx
       The syntax is the same as used with url() in @font-face rule:
        "filename.svg#font_id"
    3. Relaunch Inkscape

"""
# standard library
import os
import sys
import locale
from copy import deepcopy
from math import ceil

# compat
import six

# local library
import inkex
import cubicsuperpath
import simplestyle
import simpletransform


__version__ = '0.1'


# globals
SVGFONT_DATA = '{0}data'.format(__file__.rsplit('.', 1)[0])


# Possible workaround for encoding issues on Windows
# (see also bug #1518302)
ENCODING = sys.stdin.encoding
if ENCODING == 'cp0' or ENCODING is None:
    ENCODING = locale.getpreferredencoding()


# ----- compat with 0.91

def invertTransform(mat):
    """Return inverted mat."""
    # pylint: disable=invalid-name
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    if det != 0:  # det is 0 only in case of 0 scaling
        # invert the rotation/scaling part
        a11 = mat[1][1]/det
        a12 = -mat[0][1]/det
        a21 = -mat[1][0]/det
        a22 = mat[0][0]/det
        # invert the translational part
        a13 = -(a11*mat[0][2] + a12*mat[1][2])
        a23 = -(a21*mat[0][2] + a22*mat[1][2])
        return [[a11, a12, a13], [a21, a22, a23]]
    else:
        return [[0, 0, -mat[0][2]], [0, 0, -mat[1][2]]]


def computePointInNode(pt, node, mat=None):
    """Return (absolute) point in coordinates relative to node)."""
    # pylint: disable=invalid-name
    if mat is None:
        mat = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]
    if node.getparent() is not None:
        simpletransform.applyTransformToPoint(
            invertTransform(simpletransform.composeParents(node, mat)), pt)
    return pt

# ----- end compat with 0.91

# ----- copied from image_lib.transform

def ident_mat():
    """Return 2x3 identity matrix."""
    return [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]


def invert(mat):
    """Return inverted transformation matrix."""
    if hasattr(simpletransform, 'invertTransform'):
        return simpletransform.invertTransform(mat)
    else:
        return invertTransform(mat)


def copy_from(node):
    """Return transformation matrix for node's preserved transform."""
    return simpletransform.parseTransform(node.get('transform'))


def absolute(node):
    """Return transformation matrix transforming node to SVGRoot."""
    if node.getparent() is not None:
        return simpletransform.composeParents(node, ident_mat())
    else:
        return copy_from(node)

# ----- end of copied from image_lib.transform


def find_ext_file(fname, suffix):
    """Return absolute path of fname with suffix."""
    path = os.path.dirname(os.path.realpath(__file__))
    fpath = '{0}.{1}'.format(os.path.join(path, SVGFONT_DATA, fname), suffix)
    if os.path.isfile(fpath):
        return fpath
    else:
        return None


def find_ext_filename(filename):
    """Return absolute path of filename based on script's location."""
    path = os.path.dirname(os.path.realpath(__file__))
    fpath = os.path.join(path, SVGFONT_DATA, filename)
    if os.path.isfile(fpath):
        return fpath
    else:
        return None


def parse_svg_file(fname):
    """Parse external SVG file and return document tree."""
    stream = document = None
    filename = find_ext_filename(fname)
    if filename is not None:
        try:
            stream = open(filename, 'r')
        except IOError:
            inkex.errormsg("Unable to open specified file: %s" % filename)
    if stream is not None:
        param = inkex.etree.XMLParser(huge_tree=True)
        document = inkex.etree.parse(stream, parser=param)
        stream.close()
    return document


def find_element(node, tag, namespace='svg'):
    """Wrapper for lxml find()."""
    result = node.find(inkex.addNS(tag, namespace))
    if result is None:
        # try without SVG namespace
        result = node.find(tag)
    return result


def find_all_elements(node, tag, namespace='svg'):
    """Wrapper for lxml findall()."""
    result = node.findall(inkex.addNS(tag, namespace))
    if not len(result):
        # try without SVG namespace
        result = node.findall(tag)
    return result


def get_element_by_id_from_doc(id_, document):
    """Return element by id from document (XML tree) passed as argument."""
    path = '//*[@id="{0}"]'.format(id_)
    el_list = document.xpath(path, namespaces=inkex.NSS)
    if el_list:
        return el_list[0]
    else:
        return None


def get_fontface(doc, font_id):
    """Return SVG font element by id."""
    return get_element_by_id_from_doc(font_id, doc)


def resolve_tspan(node):
    """Recursively process <tspan> and append text content."""
    line = ""
    if node.tag == inkex.addNS('tspan', 'svg'):
        if node.text:
            line += node.text
        for child in node:
            line += resolve_tspan(child)
        if node.tail:
            line += node.tail
    return line


def get_text_lines(node):
    """Extract text content of node and return list of strings."""
    lines = origin = []
    if node.tag == inkex.addNS('text', 'svg'):
        origin = [float(node.get('x', 0)), float(node.get('y', 0))]
        if node.text:
            lines.append(node.text)
        for child in node:
            lines.append(resolve_tspan(child))
    return (lines, origin)


def ref_node(doc, node):
    """Return element linked to via href (e.g. by <use> elements)."""
    ref_id = node.get(inkex.addNS('href', 'xlink'))
    if ref_id:
        return get_element_by_id_from_doc(ref_id[1:], doc)
    else:
        return None


def ungroup_path(group):
    """Recursively ungroup and combine contained <path> elements."""
    if group.get('transform'):
        mat = simpletransform.parseTransform(group.get('transform'))
    else:
        mat = ident_mat()
    glyph_csp = cspp = []
    for child in group:
        if child.tag == inkex.addNS('desc', 'svg'):
            pass
        elif child.tag == inkex.addNS('g', 'svg'):
            cspp = ungroup_path(child)
            simpletransform.applyTransformToPath(mat, cspp)
        elif child.tag == inkex.addNS('path', 'svg'):
            if child.get('d'):
                cspp = cubicsuperpath.parsePath(child.get('d'))
                simpletransform.applyTransformToPath(mat, cspp)
        if len(cspp):
            for subp in cspp:
                glyph_csp.append(subp)
    return glyph_csp


def resolve_use(doc, node):
    """Recursively process <use> elements and convert into groups."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    if node.get('transform'):
        mat = simpletransform.parseTransform(node.get('transform'))
        simpletransform.applyTransformToNode(mat, group)
    mat = simpletransform.parseTransform(
        'translate({0},{1})'.format(node.get('x', 0), node.get('y', 0)))
    simpletransform.applyTransformToNode(mat, group)
    node_orig = ref_node(doc, node)
    if node_orig is not None:
        if node_orig.tag == inkex.addNS('use', 'svg'):
            use_group = resolve_use(doc, node_orig)
            group.append(use_group)
        elif node_orig.tag == inkex.addNS('g', 'svg'):
            sub_group = resolve_group(doc, node_orig)
            group.append(sub_group)
        else:
            group.append(deepcopy(node_orig))
    else:
        inkex.errormsg('Orphaned clone ({0}) ignored.'.format(node.get('id')))
    return group


def resolve_group(doc, node):
    """Recursively process groups, follow links of <use> childs."""
    group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    if node.get('transform'):
        group.set('transform', node.get('transform'))
    for child in node:
        if child.tag == inkex.addNS('use', 'svg'):
            use_group = resolve_use(doc, child)
            group.append(use_group)
        elif child.tag == inkex.addNS('g', 'svg'):
            sub_group = resolve_group(doc, child)
            group.append(sub_group)
        else:
            group.append(deepcopy(child))
    return group


def get_glyph_group(face, glyph):
    """Return group with copy of the <glyph>'s content."""
    fontdoc = face.getroottree()
    glyph_group = inkex.etree.Element(inkex.addNS('g', 'svg'))
    if glyph.get('d'):
        glyph_path = inkex.etree.Element(inkex.addNS('path', 'svg'))
        glyph_path.set('d', glyph.get('d'))
        glyph_group.append(glyph_path)
    if len(glyph) > 0:
        for child in glyph:
            if child.tag == inkex.addNS('use', 'svg'):
                use_group = resolve_use(fontdoc, child)
                glyph_group.append(use_group)
            elif child.tag == inkex.addNS('g', 'svg'):
                sub_group = resolve_group(fontdoc, child)
                glyph_group.append(sub_group)
            else:  # copy everything which is not a container or instance
                glyph_group.append(deepcopy(child))
    return glyph_group


def draw_svgfont_text(char, face, h_offset, v_offset, parent, style, mode):
    """Add glyph group or path for unicode char from font face."""
    # pylint: disable=too-many-arguments
    face_advance_x = face.get('horiz-adv-x', '0')
    if char == u'"':
        glyph = face.xpath(u'*[@unicode=\'"\']')
    else:
        glyph = face.xpath(u'*[@unicode="{0}"]'.format(char))
    if len(glyph):
        glyph = glyph[0]
    else:
        glyph = find_element(face, 'missing-glyph')
    if glyph is not None:
        advance_x = float(glyph.get('horiz-adv-x', face_advance_x))
        glyph_group = get_glyph_group(face, glyph)
        if glyph_group is not None:
            mat = simpletransform.parseTransform(
                "scale(1,-1)")
            simpletransform.applyTransformToNode(mat, glyph_group)
            mat = simpletransform.parseTransform(
                'translate({0},{1})'.format(h_offset, v_offset))
            simpletransform.applyTransformToNode(mat, glyph_group)
            # Append glyph to parent, add style
            if mode == 'group':
                # Add desc with Unicode character and code point (int, hex)
                group_desc = inkex.etree.Element(inkex.addNS('desc', 'svg'))
                if len(char) == 1:
                    group_desc.text = u'{0} (ord: {1}) (U+{1:04X})'.format(
                        char, ord(char))
                else:
                    group_desc.text = u'{0}'.format(char)
                glyph_group.insert(0, group_desc)
                # Set style
                glyph_group.set('style', simplestyle.formatStyle(style))
                # Append to parent
                parent.append(glyph_group)
            elif mode == 'path':
                glyph_csp = ungroup_path(glyph_group)
                if glyph_csp:
                    glyph_path = inkex.etree.Element(inkex.addNS('path', 'svg'))
                    glyph_path.set('d', cubicsuperpath.formatPath(glyph_csp))
                    # Set style
                    glyph_path.set('style', simplestyle.formatStyle(style))
                    # Append to parent
                    parent.append(glyph_path)
        # glyph found, advance per glyph
        return h_offset + advance_x
    # no glyph found, just advance
    return h_offset + face_advance_x


def apply_scale(node, scale):
    """Apply scale to node."""
    if scale != 1.0:
        mat = simpletransform.parseTransform('scale({0})'.format(scale))
        simpletransform.applyTransformToNode(mat, node)


def apply_translate(node, x, y):
    """Apply translation to node."""
    # pylint: disable=invalid-name
    if x != 0 or y != 0:
        mat = simpletransform.parseTransform('translate({0},{1})'.format(x, y))
        simpletransform.applyTransformToNode(mat, node)


class DrawText(inkex.Effect):
    """Effect-based class to draw text."""

    def __init__(self):
        """Init base class and parse options for DrawText class."""
        inkex.Effect.__init__(self)

        # instance attributes
        self.scale = 1.0            # drawing scale
        self.font_document = None   # ElementTree
        self.font = None            # <font>
        self.font_face = None       # <font-face>
        self.fontscale = None       # scale factor for font size based on EM
        self.style = {}             # output style

        # input
        self.OptionParser.add_option("--text",
                                     action="store",
                                     type="string",
                                     dest="text",
                                     default="Draw Text for Inkscape",
                                     help="Text to render")
        self.OptionParser.add_option("--action",
                                     action="store",
                                     type="string",
                                     dest="action",
                                     default="render",
                                     help="Selected action")
        self.OptionParser.add_option("--keep_original",
                                     action="store",
                                     type="inkbool",
                                     dest="keep_original",
                                     default=True,
                                     help="Keep original text")
        self.OptionParser.add_option("--fontface",
                                     action="store",
                                     type="string",
                                     dest="fontface",
                                     default="ISO_3098_CBPV",
                                     help="Selected font face")
        self.OptionParser.add_option("--fontfaceurl",
                                     action="store",
                                     type="string",
                                     dest="fontfaceurl",
                                     default="",
                                     help="Custom SVG font")
        # output
        self.OptionParser.add_option("--mode",
                                     action="store",
                                     type="string",
                                     dest="mode",
                                     default="group",
                                     help="Output mode")
        self.OptionParser.add_option("--fontsize_unit",
                                     action="store",
                                     type="string",
                                     dest="fontsize_unit",
                                     default="px",
                                     help="Font size unit")
        self.OptionParser.add_option("--fontsize",
                                     action="store",
                                     type="float",
                                     dest="fontsize",
                                     default=10.0,
                                     help="Font size")
        self.OptionParser.add_option("--linespacing",
                                     action="store",
                                     type="float",
                                     dest="linespacing",
                                     default=1.25,
                                     help="Line spacing")
        self.OptionParser.add_option("--kern",
                                     action="store",
                                     type="inkbool",
                                     dest="kern",
                                     default=True,
                                     help="Use kerning")
        self.OptionParser.add_option("--style_stroked",
                                     action="store",
                                     type="inkbool",
                                     dest="style_stroked",
                                     default=True,
                                     help="Output style")
        self.OptionParser.add_option("--style_filled",
                                     action="store",
                                     type="inkbool",
                                     dest="style_filled",
                                     default=False,
                                     help="Output style")
        # about
        self.OptionParser.add_option("--source_iso3098",
                                     action="store",
                                     type="string",
                                     dest="source_iso3098",
                                     default="",
                                     help="URL for SVG source file")
        # tabs
        self.OptionParser.add_option("--tab",
                                     action="store",
                                     type="string",
                                     dest="tab",
                                     help="Active tab")

    def add_desc(self, node, alist):
        """Add <desc> to node with catenated content of alist children."""
        # pylint: disable=no-member
        node_desc = inkex.etree.Element(inkex.addNS('desc', 'svg'))
        # NOTE python3: NameError: name 'unicode' is not defined
        if six.PY2:
            node_desc.text = unicode('\n'.join(alist))
        else:
            node_desc.text = self.to_unicode('\n'.join(alist))
        node.insert(0, node_desc)


    def configure_font(self):
        """Store font parameters as instance attributes."""
        # Document scale
        self.scale = self.unittouu('1px')
        # Font data
        if self.options.fontface == 'custom':
            self.options.fontface = self.options.fontfaceurl
        filename, svgfont_id = self.options.fontface.split('#')
        self.font_document = parse_svg_file(filename)
        if self.font_document is not None:
            self.font = get_fontface(self.font_document, svgfont_id)
        # Font scale
        if self.font is not None:
            self.font_face = find_element(self.font, 'font-face')
            if self.font_face is None:
                self.font = None
            else:
                units_per_em = self.font_face.get('units-per-em', 1000.0)
                unit_factor = self.scale / self.unittouu('1{0}'.format(
                    self.options.fontsize_unit))
                fontsize = self.options.fontsize / unit_factor
                self.fontscale = fontsize / float(units_per_em)

    def configure_style(self):
        """Store output style in instance attribute."""
        # Define default style
        self.style = {'fill': 'none', 'stroke': 'none'}
        # Adjust based on input and output options
        if self.options.style_stroked:
            self.style['stroke'] = '#000000'
        if self.options.style_filled:
            self.style['fill'] = '#000000'
        if self.font.get('id').startswith('ISO-3098'):
            self.style['stroke-linecap'] = 'round'
            self.style['stroke-linejoin'] = 'round'

    def create_group(self, label="DrawText"):
        """Add new group with label to document."""
        group_attribs = {inkex.addNS('label', 'inkscape'): label}
        group = inkex.etree.SubElement(
            self.current_layer, inkex.addNS('g', 'svg'), group_attribs)
        return group

    def center_in_viewport(self, node, max_x, max_y):
        """Center node in viewport, approximately."""
        view_center = computePointInNode(
            list(self.view_center), self.current_layer)
        if max_x > 0:
            mat = simpletransform.parseTransform(
                'translate({0},{1})'.format(
                    view_center[0] - self.fontscale*self.scale*max_x/2,
                    view_center[1] - self.fontscale*self.scale*max_y/2))
            simpletransform.applyTransformToNode(mat, node)

    def lookup_hkern(self, char1, char2):
        """Look up matching pairs in <hkern> lists, return k."""
        # TODO: support other allowed Unicode codepoint notations for u1, u2
        # TODO: support looking up pairs in g1, g2
        for hkern in find_all_elements(self.font, 'hkern'):
            if char1 in hkern.get('u1').split(','):
                if char2 in hkern.get('u2').split(','):
                    return float(hkern.get('k'))
        return 0

    def draw_text_char(self, char, h_offset, v_offset, group):
        """Draw char of text."""
        return draw_svgfont_text(
            char, self.font, h_offset, v_offset,
            group, self.style, self.options.mode)

    def draw_text_line(self, line, h_offset, v_offset, group):
        """Draw line of text."""
        # pylint: disable=no-member
        prev_char = None
        # NOTE python3: NameError: name 'unicode' is not defined
        if six.PY2:
            unicode_chars = [unicode(char) for char in line]
        else:
            unicode_chars = [self.to_unicode(char) for char in line]
        for char in unicode_chars:
            if prev_char is not None:
                if self.options.kern:
                    h_offset -= self.lookup_hkern(prev_char, char)
            h_offset = self.draw_text_char(char, h_offset, v_offset, group)
            prev_char = char
        return h_offset

    def draw_text_lines(self, lines, group, text_x=0, text_y=0):
        """Draw text with glyph outlines taken from external SVG font."""
        h_offset = v_offset = max_x = 0.0
        line_height = self.options.linespacing * float(
            self.font_face.get('units-per-em', 1000.0))
        # Draw each line (regular character spacing from font)
        for line in lines:
            last_x = self.draw_text_line(line, h_offset, v_offset, group)
            max_x = max(last_x, max_x)
            v_offset += line_height
        # Apply font scale
        apply_scale(group, self.fontscale)
        # Apply drawing scale
        apply_scale(group, self.scale)
        # Translate group to text origin or center of viewport
        if text_x != 0 or text_y != 0:
            apply_translate(group, text_x, text_y)
        else:
            self.center_in_viewport(group, max_x, v_offset - line_height)
        # Add desc with text content
        self.add_desc(group, lines)
        return group

    def render_text(self):
        """Render text from direct input."""
        # NOTE python3: AttributeError: 'str' object has no attribute 'decode'
        if six.PY2:
            lines = [self.options.text.decode(ENCODING)]
        else:
            lines = [self.options.text]
        group = self.create_group()
        self.draw_text_lines(lines, group)

    def convert_text(self):
        """Convert text content from selected text objects."""
        if len(self.selected):
            count = 0
            # NOTE python3: AttributeError: 'dict' object has no attribute 'iteritems'
            for id_, node in six.iteritems(self.selected):
                lines, text_origin = get_text_lines(node)
                if len(lines):
                    count += 1
                    label = '{0}_converted'.format(id_)
                    group = self.create_group(label)
                    self.draw_text_lines(lines, group, *text_origin)
                    simpletransform.applyTransformToNode(
                        simpletransform.composeTransform(
                            invert(absolute(group.getparent())),
                            absolute(node)),
                        group)
                    if not self.options.keep_original:
                        node.getparent().remove(node)
            if not count:
                inkex.errormsg("No text content found in selection.")
        else:
            inkex.errormsg(
                "This mode requires one or more selected objects.")

    def draw_table(self):
        """Draw table with available glyphs of selected font."""
        self.options.kern = False
        # Get all glyphs from SVG font
        glyphs = find_all_elements(self.font, 'glyph')
        # Configure layout
        columns = 20
        rows = int(ceil(len(glyphs)/float(columns)))
        spacing = 2 * float(self.font_face.get('units-per-em', 1000.0))
        padding = 0.25
        # Prepare group for table
        group = self.create_group(label="table")
        apply_scale(group, self.fontscale)
        apply_scale(group, self.scale)
        # Draw glyphs
        count = 0
        v_offset = -padding * spacing
        for row in range(rows):
            h_offset = padding * spacing
            v_offset += spacing
            for column in range(columns):
                if count < len(glyphs):
                    glyph = glyphs[count]
                    if 'unicode' in glyph.attrib:
                        self.draw_text_char(
                            glyph.get('unicode'), h_offset, v_offset, group)
                    h_offset += spacing
                count += 1
        # Draw grid
        group = self.create_group(label="grid")
        grid_style = 'stroke:blue;stroke-width:{0}px'.format(0.5*self.scale)
        group.set('style', grid_style)
        spacing = (2.0 * self.options.fontsize *
                   self.unittouu('1{0}'.format(self.options.fontsize_unit)))
        for row in range(rows+1):
            line = inkex.etree.SubElement(group, inkex.addNS('path', 'svg'))
            line.set('d', 'M {0},{1} {2},{1}'.format(
                0, row*spacing, columns*spacing))
        for column in range(columns+1):
            line = inkex.etree.SubElement(group, inkex.addNS('path', 'svg'))
            line.set('d', 'M {0},{1} {0},{2}'.format(
                column*spacing, 0, rows*spacing))

    def effect(self):
        """Main dispatcher for DrawText effect."""
        if self.options.fontface != 'none':
            self.configure_font()
        if self.font is not None:
            self.configure_style()
            if self.options.action == "render":
                self.render_text()
            elif self.options.action == 'convert':
                self.convert_text()
            elif self.options.action == 'table':
                self.draw_table()
        else:
            inkex.errormsg(
                'Failed to load selected font:' +
                '\n{0}'.format(self.options.fontface))


if __name__ == '__main__':
    ME = DrawText()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
